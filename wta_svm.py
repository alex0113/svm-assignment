from copy import deepcopy
from svm_classifier import SVMClassifier

class WTA_SVM():
    def __init__(self, y, x, param_str):
        self.classes = []
        self.classifiers = []
        self.size = len(y)
        for label in y:
            if not label in self.classes:
                self.classes.append(label)

        for sep_class in self.classes:
            new_y = self.__separate_binary(y, sep_class)
            self.classifiers.append(SVMClassifier(new_y, x, param_str))

    def train(self):
        for classifier in self.classifiers:
            classifier.train()

    def predict(self, y_test, x_test):
        for classifier in self.classifiers:
            classifier.predict(y_test, x_test)

        predicted_labels = []
        for i in range(self.size):
            values = [classifier.probability(i) for classifier in self.classifiers]
            predicted_labels.append(self.__find_prediction(values))

        return self.__compute_accuracy(predicted_labels, y_test)

    def __separate_binary(self, y, sep_class):
        y = deepcopy(y)

        s = len(y)
        for i in range(s):
            if not y[i] == sep_class:
                y[i] = -1

        return y
    def __find_prediction(self, values):
        max_v = -999
        max_class = -1
        for prob, p_class in values:
            if prob > max_v and not p_class == -1:
                max_v = prob
                max_class = p_class

        return max_class

    def __compute_accuracy(self, predicted_labels, y_test):
        correct = 0
        for i in range(self.size):
            if predicted_labels[i] == y_test[i]:
                correct += 1

        accuracy = correct/float(self.size)
        return accuracy
