from svmutil import svm_parameter, svm_read_problem, svm_problem, svm_train, svm_predict
class SVMClassifier():
    def __init__(self, y, x, param_str):
        print("FDs")
        self.param = svm_parameter(param_str)
        self.prob = svm_problem(y, x)


    def train(self):
        self.model= svm_train(self.prob, self.param)

    def predict(self, y_test, x_test):
        p_labels, p_acc, p_vals = svm_predict(y_test, x_test, self.model)
        #print(len(p_labels), len(p_vals), len(y_test))
        #print(type(p_labels), p_acc, type(p_vals))
        #print(p_labels[:10])
        #print(p_vals[:10])
        self.p_vals = p_vals
        self.p_labels = p_labels

    def probability(self, index):
        return (self.p_vals[index][0], self.p_labels[index])
    
