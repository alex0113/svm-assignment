from svmutil import LINEAR, POLY, RBF,\
svm_parameter, svm_read_problem, svm_problem, svm_train, svm_predict
y, x = svm_read_problem('mushrooms')
chunk = 1500
print(len(x[1]))
prob = svm_problem(y[chunk:], x[chunk:])
param = svm_parameter('-t 2')
#param.gamma = 1/21
#param.kernel_tye = POLY
# Compute GRAM matrix with custom kernel
model= svm_train(prob, param)
p_labels, p_acc, p_vals = svm_predict(y[:chunk], x[:chunk], model)
print(type(p_labels), p_acc, type(p_vals))
