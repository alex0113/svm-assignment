import numpy as np
import numpy.linalg as la

"""Implementeaza o serie de kernel-uri bine cunoscute: liniar, RBF
"""

def linear():
    """
    Intoarce o functie anonima, in doi parametrii, x si y, ce calculeaza produsul scalar a doi vectori x si y
    :return:
    """
    g = lambda x,y: np.inner(x, y)
    return g


def radial_basis(gamma=10):
    """
    Intoarce o functie anonima, in doi parametrii, x si y, ce implementeaza forma de Radial Basis Function,
    avand parametrul \gamma
    :param gamma: parametrul de ponderare a normei diferentei vectorilor x si y
    :return:
    """

    g = lambda x,y: np.exp(-gamma * la.norm(np.subtract(x, y)))
    return g

def gaussian_rbf(sigma=10):
    """
    Intoarce o functie anonima, in doi parametrii, x si y, ce implementeaza forma de Radial Basis Function,
    avand parametrul \gamma
    :param gamma: parametrul de ponderare a normei diferentei vectorilor x si y
    :return:
    """
    g = lambda x,y: np.exp(-1/(2*sigma*sigma) * la.norm(np.subtract(x, y)))
    return g

def polynomial(sigma=10, d = 10, theta = 5):
    g = lambda x, y: sigma*pow(np.inner(x, y) + theta, d)

def preprocess_data(x):
    max_key=np.max([np.max(v.keys()) for v in x])
    arr=np.zeros( (len(x),max_key) )
    for row,vec in enumerate(x):
        for k,v in vec.iteritems():
            arr[row][k-1]=v
    X=arr

    return X

def gram_matrix(kernel, X):
        """
        Precalculeaza matricea Gram, folosind kernel-ul dat in constructor, in vederea rezolvarii problemei duale.
        :param X: setul de date de antrenare avand dimesiunea (num_samples, num_features)
        :return: Matricea Gram precalculata
        """
        n_samples = len(X)
        K = [{} for i in range(n_samples)]

        # TODO: populati matricea Gram conform kernel-ului selectat
        for i in range(n_samples):
            print(i)
            K[i][0] = i+1
            for j in range(n_samples):
                K[i][j+1] = kernel(X[i].keys(), X[j].keys())
        print("here")
        return K
