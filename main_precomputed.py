from svmutil import LINEAR, POLY, RBF,\
svm_parameter, svm_read_problem, svm_problem, svm_train, svm_predict
from kernels import gram_matrix, linear, preprocess_data, radial_basis, gaussian_rbf

y, x = svm_read_problem('mushrooms')
#print(X[1])
#quit()
size = len(x)
chunk = size / 20
chunk = 2000

print("Preprocess")
#x = preprocess_data(X)
print("Gram")
K_train = gram_matrix(linear(),x[chunk:chunk*2])
K_test = gram_matrix(linear(), x[:chunk])

print("Problem", K_train[0])
prob = svm_problem(y[chunk:chunk*2], K_train)
param = svm_parameter('-t 4 -c 4 -b 1')
#param.kernel_type = POLY
param.C = 10
# Compute GRAM matrix with custom kernel
model= svm_train(prob, param)
print("SVM predict")
p_labels, p_acc, p_vals = svm_predict(y[:chunk], K_test, model)
print(type(p_labels), p_acc, type(p_vals))
