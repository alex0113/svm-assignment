from svmutil import LINEAR, POLY, RBF,\
svm_parameter, svm_read_problem, svm_problem, svm_train, svm_predict
from svm_classifier import SVMClassifier
from wta_svm import WTA_SVM

y, x = svm_read_problem('pendigits')
y_test, x_test = svm_read_problem('pendigits.t')
#svm_classifier = SVMClassifier(y, x, '-t 0 -b 1')
#svm_classifier.train()
#svm_classifier.predict(y_test, x_test)
#print(type(p_labels), p_acc, type(p_vals))
#print(p_labels)
wta_svm = WTA_SVM(y[:300], x[:300], '-t 0 -b 1')
wta_svm.train()
accuracy = wta_svm.predict(y_test[:300], x_test[:300])
print(accuracy)
