from svmutil import *
import numpy as np
from kernels import gram_matrix, linear

#original example
#y, x = svm_read_problem('.../heart_scale')
y, x = svm_read_problem('mushrooms')
m = svm_train(y[:200], x[:200], '-c 4')
p_label, p_acc, p_val = svm_predict(y[200:], x[200:], m)

print("Precomputed")
##############
#train the SVM using a precomputed linear kernel

#create dense data
max_key=np.max([np.max(v.keys()) for v in x])
arr=np.zeros( (len(x),max_key) )

#print("x", x)
print(type(x), len(x), type(x[0]), len(x[0]))
print(x[0].keys())
print(x[1].keys())
for row,vec in enumerate(x):
    for k,v in vec.iteritems():
        arr[row][k-1]=v
x=arr
print("x", x)
K_train = gram_matrix(linear(),x[:200])
#create a linear kernel matrix with the training data
#K_train=np.zeros( (200,201) )
#K_train[:,1:]=np.dot(x[:200],x[:200].T)
#K_train[:,:1]=np.arange(200)[:,np.newaxis]+1

print(K_train)
m = svm_train(y[:200], [list(row) for row in K_train], '-c 4 -t 4')

#create a linear kernel matrix for the test data
K_test=np.zeros( (len(x)-200,201) )
K_test[:,1:]=np.dot(x[200:],x[:200].T)
K_test[:,:1]=np.arange(len(x)-200)[:,np.newaxis]+1

p_label, p_acc, p_val = svm_predict(y[200:],[list(row) for row in K_test], m)
